﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spriteChanger : MonoBehaviour {

	public SpriteRenderer sr;
	public Transform tf;
	public float speed;

	// Use this for initialization
	void Start () {
		sr = GetComponent<SpriteRenderer> ();
		tf = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Space)) {
			sr.color = Color.red;
		} else {
			sr.color = Color.white;
		}

		if (Input.GetKey (KeyCode.RightArrow)) {
			// Moving right
			tf.position = tf.position + (Vector3.right * speed);
		}
		if (Input.GetKey (KeyCode.LeftArrow)) {
			
			// Moving left
			tf.position = tf.position + (Vector3.left * speed);
		}
	}
}